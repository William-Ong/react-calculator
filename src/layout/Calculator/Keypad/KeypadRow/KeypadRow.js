import React from 'react';

/*
This property is instantiated when nesting components. Since KeypadRow
was nesting the Button components, the children property became the button
elements (4 buttons to be precise). Since we wanted them all to be rendered
on the page, we called 'props.children' directly, without altering anything.
*/ 
const keypadRow = (props) => (
    <div className="keypad__row">
        {props.children}
    </div>
);

export default keypadRow;