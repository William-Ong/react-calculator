import React from 'react';

import Screen from './Screen/Screen'
import Keypad from './Keypad/Keypad'

class Calculator extends React.Component {
    /* Default state values */
    state = {
        equation: '',
        result: 0
    }

    /* Button event listener and handler */
    onButtonPress = event => {
        let equation = this.state.equation;
        const pressedButton = event.target.innerHTML;

        if(pressedButton === 'C') 
        {
            return this.clear();
        }
        else if((pressedButton >= '0' && pressedButton <= '9') || (pressedButton === '.'))
        {
            equation += pressedButton;
        }
        else if(['+', '-', '*', '/', '%'].indexOf(pressedButton) !== -1)
        {
            equation += ' ' + pressedButton + ' ';
        }
        else if(pressedButton === '=')
        {
            try
            {
                /**
                 * TODO: Switch out eval() as it is subjected to harmful security vulnerabilities
                 */
                const evalResult = eval(equation);
                /**
                 * If result is interger, pass back the result as it is, otherwise round up the
                 * decimal point to 2. Set the new state of result.
                 */
                const result = Number.isInteger(evalResult) ? evalResult : evalResult.toFixed(2);
                this.setState({result});
            }
            catch(error)
            {
                alert('Invalid Mathematical Equation!');
            }
        }
        else
        {
            /**
             * Removes whitespace from equation and extract the entire equation
             */
            equation = equation.trim();
            equation = equation.substr(0, equation.length - 1);
        }
        /* Keeps updating the computation screen */
        this.setState({equation: equation});
    }

    /* Clear and set state to default */
    clear() {
        this.setState({equation: '', result: 0});
    }

    /* Render the actual calculator */
    /**
     * Send onButtonPress properties to its children
     */
    render() {
        return (
            <main className="calculator">
                <Screen equation={this.state.equation} result={this.state.result} />
                <Keypad onButtonPress={this.onButtonPress} /> 
            </main>
        );
    }
}

export default Calculator;