import React from 'react';

const button = (props) => {
    /* Initialize classes array with base button */
    const classes = ['btn'];

    /* 
        Check if a type named prop is passed from the parent, if it is then
        we add the passed prop prefixed with btn-- to our classes array.   
    */
    if(typeof props !== 'undefined' && typeof props.type !== 'undefined')
        classes.push('btn--' + props.type);

    return (
        /*
            Assign the joined classes to the className attribute, which this allows
            us to dynamically control the styling of a component.
            
            Curly braces are required to execute the classes.join() Javascript expression
            in the JSX markup.
        */
        <button className={classes.join(' ')} onClick={props.onButtonPress}>
        {props.children}
    </button>
    );
}

export default button;